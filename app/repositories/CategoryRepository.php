<?php
interface CategoryRepository {
	public function getAllCategories();
	public function RemoveCategory($category_id);
	public function EditCategory($category_id,$name);
	public function NewCategory($name);
}
?>