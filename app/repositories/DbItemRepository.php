<?php
class DbItemRepository extends \Exception implements ItemRepository{
	public function __construct($message = '', $error_code = null)
	{

	}
	public function getItem($skip = 0, $take = 10)
	{
		return Item::skip($skip)->take($take)->get();
	}
	public function RemoveItem($item_id)
	{
		$item = Item::find($item_id);
		if(empty($item))
			throw new Exception(STR_ERROR_ITEM_NOT_FOUND, 4);
		else
			$item->delete();
	}
}
?>