<?php
interface ItemRepository{
	public function getItem($skip = 0, $take = 10);
	public function RemoveItem($item_id);
}
?>