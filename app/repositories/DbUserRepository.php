<?php
class DbUserRepository extends \Exception implements UserRepository{
	public function __construct($message = '',$error_code = null)
	{

	}
	/*
	* get users per page, with $number_user_per_page user each page
	*/
	public function getUser($skip = 0,$take = 10)
	{
		return User::skip($skip)->take($take)->get();
	}
	/*
	* Deactive user
	*/
	public function ChangeUserStatus($user_id,$status = DEACTIVE){
		$vali = ['user_id' => $user_id];
		$rules = ['user_id' =>['required','regex:/^[a-zA-Z0-9_]+$/']];
		if(Validator::make($vali,$rules)->fails())
			throw new Exception(STR_ERROR_VALIDATE, 1);
		else
			$user = User::find($user_id);
			if(empty($user))
				throw new Exception(STR_ERROR_USER_NOT_FOUND, 2);
			else
				$user->active = $status;
				$user->save();
	}
	public function ChangeUserPermission($user_id,$permission = 0)
	{
		$vali  = ['user_id' => $user_id];
		$rules = ['user_id' =>['required','regex:/^[a-zA-Z0-9_]+$/']];
		if(Validator::make($vali,$rules)->fails())
			throw new Exception(STR_ERROR_VALIDATE, 1);
		else
			$user = User::find($user_id);
			if(empty($user))
				throw new Exception(STR_ERROR_USER_NOT_FOUND, 2);
			else
				$user->permission = $permission;
				$user->save();
	}
	public function EditUsername($user_id,$username)
	{
		$user = User::find($user_id);
		if(empty($user))
			throw new Exception(STR_ERROR_USER_NOT_FOUND, 2);
		else
			if(User::where('username','=',$username)->count() > 0)
				throw new Exception(STR_ERROR_USERNAME_EXIST, 5);
			else
				$user->username = $username;
				$user->save();
			
	}
	public function EditEmail($user_id,$email)
	{
		$vali = ['email' => $email];
		$rules = ['email' => 'email'];
		if(Validator::make($vali,$rules)->fails())
			throw new Exception(STR_ERROR_VALIDATE_EMAIL, 7);
		else
			$user = User::find($user_id);
			if(empty($user))
				throw new Exception(STR_ERROR_USER_NOT_FOUND, 2);
			else
				if(User::where('email','=',$email)->count() > 0)
					throw new Exception(STR_ERROR_EMAIL_EXIST, 6);
				else
					$user->email = $email;
					$user->save();
				
	}
}

?>