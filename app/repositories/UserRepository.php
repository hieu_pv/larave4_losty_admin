<?php
interface UserRepository{
	public function getUser($skip = 0,$take = 10);
	public function ChangeUserStatus($user_id,$status = DEACTIVE);
	public function ChangeUserPermission($user_id,$permission = 0);
	public function EditUsername($user_id,$username);
	public function EditEmail($user_id,$email);
}
?>