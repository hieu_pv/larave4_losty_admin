<?php
class DbCategoryRepository extends \Exception implements CategoryRepository{
	public function __construct($message = '',$error_code = null)
	{

	}
	public function getAllCategories(){
		return Categories::get();
	}
	public function RemoveCategory($category_id){
		$category = Categories::find($category_id);
		if(empty($category))
			throw new Exception(STR_ERROR_CATEGORY_NOT_FOUND, 3);
		else
			$category->delete();
			
	}
	public function EditCategory($category_id,$name){
		$category = Categories::find($category_id);
		if(empty($category))
			throw new Exception(STR_ERROR_CATEGORY_NOT_FOUND, 3);
		else
			$category->name = $name;
			$category->save();
			
	}
	public function NewCategory($name){
		$category = new Categories();
		$category->name = $name;
		$category->save();
	}
}
?>