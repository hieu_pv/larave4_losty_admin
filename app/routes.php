<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::filter('login',function(){
	if(!Session::has('login') || Session::get('login') != 1){
		return Redirect::to('login');
	}
});
Route::when('user','login');
Route::when('category','login');
Route::when('item','login');
Route::when('/','login');
Route::controller('/','AdminController');




App::bind('UserRepository','DbUserRepository');
App::bind('CategoryRepository','DbCategoryRepository');
App::bind('ItemRepository','DbItemRepository');