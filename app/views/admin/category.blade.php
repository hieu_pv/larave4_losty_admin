@extends('admin.template')
@section('content')
<script type="text/javascript">
	var categories               = {{$categories}};
	var STR_URL_REMOVE_CATEGORY  = "{{asset('ajax/remove-category')}}";
	var STR_URL_EDIT_CATEGORY    = "{{asset('ajax/edit-category')}}";
	var STR_URL_ADD_NEW_CATEGORY = "{{asset('ajax/new-category')}}";
</script>
<link rel="stylesheet" href="{{asset('lib/css/admin/category.css')}}">
<script type="text/javascript" src="{{asset('lib/js/admin/category.js')}}"></script>
	<div class="container" ng-controller="CategoryCtrl">
		<div class="col-md-12 col-sm-18 col-xs-36 col-md-offset-12 col-sm-offset-9">	
			<button class="btn btn-default btn-block" ng-click="AddNewCategory()">Add New Category</button>
			<br>
			<ul class="list-group">
				<li class="list-group-item" ng-repeat="category in categories">
					<span ng-bind="category.name"></span>
					<span class="pull-right glyphicon glyphicon-remove" ng-click="RemoveCategory(category)"></span>
					<span class="pull-right glyphicon glyphicon-edit" ng-click="EditCategory(category)"></span>
				</li>
			</ul>
		</div>
	</div> <!-- container -->
@stop