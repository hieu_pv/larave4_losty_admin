<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>
	 
	 <!-- jQuer -->
	 <script type="text/javascript" src="{{asset('lib/js/jquery-2.1.1.min.js')}}"></script>

	 <!-- Angular -->
	 <script type="text/javascript" src="{{asset('lib/js/angular.min.js')}}"></script>


	 <script type="text/javascript" src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>	
    <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/bootstrap/css/bootstrap-theme.min.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('lib/js/admin/template.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('lib/css/admin/template.css')}}">
  </head>
  <body ng-app="Losty">
  	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="{{asset('')}}">Home</a>
			</div> <!-- navbarheader -->
			<div class="collapse navbar-collapse" id="navbar_collapse">
				<ul class="nav navbar-nav">
					<li><a href="{{asset('user')}}">User</a></li>
					<li><a href="{{asset('category')}}">Category</a></li>
					<li><a href="{{asset('item')}}">Item</a></li>
				</ul>
				<ul class="nav navbar-right">
					<li><a href="{{asset('logout')}}">Logout</a></li>
				</ul>
			</div>
		</div>
  	</nav>
   @yield('content')
  </body>
</html>