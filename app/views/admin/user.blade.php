@extends('admin.template')
@section('content')
<script type="text/javascript" src="{{asset('lib/js/admin/user.js')}}"></script>
<script type="text/javascript">
	var users                      = {{$users}};
	var pages                      = {{json_encode($pages)}};
	var SUCCESS                    = "{{STR_STATUS_SUCCESS}}";
	var STR_URL_CHANGE_USER_STATUS = "{{asset('ajax/change-user-status')}}";
	var STR_CHANGE_USER_PERMISSION = "{{asset('ajax/change-user-permission')}}";
	var STR_EDIT_USERNAME          = "{{asset('ajax/edit-username')}}";
	var STR_EDIT_EMAIL          = "{{asset('ajax/edit-email')}}";
	var ACTIVE                     = {{ACTIVE}};
	var DEACTIVE                   = {{DEACTIVE}};
	var ADMIN                      = 1;
	var USER                       = 0; 
</script>
<link rel="stylesheet" type="text/css" href="{{asset('lib/css/admin/user.css')}}">
<div class="container" ng-controller="UserCtrl">
	<table class="table table-bordered">
		<thead>
			<tr>
				<td>Username</td>
				<td>Email</td>
				<td>Permission</td>
				<td>Active/Deactive</td>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="user in users">
				<td><span ng-bind="user.username"></span><span class="pull-right glyphicon glyphicon-edit" ng-click="EditUsername(user)"></span></td>
				<td><span ng-bind="user.email"></span><span class="pull-right glyphicon glyphicon-edit" ng-click="EditEmail(user)"></span></td>
				<td>
					<span ng-if="user.permission == USER">user</span>
					<span ng-if="user.permission == ADMIN"><strong>Admin</strong></span>
					<span ng-click="ChangeUserPermission(user)" class="change-user-permission pull-right glyphicon glyphicon-random"></span>
				</td>
				<td class="text-center user-status">
					<span style="color: green" class="glyphicon glyphicon-ok" ng-if="user.active == 1" ng-click="ChangeUserStatus(user,DEACTIVE)"></span>
					<span style="color: #c0392b" class="glyphicon glyphicon-remove" ng-if="user.active == 0" ng-click="ChangeUserStatus(user,ACTIVE)"></span>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="col-md-36">
		<ul class="pagination">
			<li ng-repeat="page in pages"><a ng-href="<%page.link%>" ng-bind="page.key"></a></li>
		</ul> <!-- pagination -->
	</div>
</div> <!-- container -->
@stop