<script type="text/javascript" src="{{asset('lib/js/jquery-2.1.1.min.js')}}"></script>

	 <!-- Angular -->
	 <script type="text/javascript" src="{{asset('lib/js/angular.min.js')}}"></script>


	 <script type="text/javascript" src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>	
    <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/bootstrap/css/bootstrap-theme.min.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('lib/js/admin/template.js')}}"></script>
	<div class="container">
		<div class="col-md-12 col-sm-18 col-xs-36 col-md-offset-12 col-sm-offset-9">
			<h1 class="text-center">Login</h1>
			@if(isset($error))
				<span>{{$error}}</span>
			@endif
			<form method="POST">
				<div class="form-group">
					<input type="text" class="form-control" name="username" placeholder="Username">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="Password">
				</div>
				<div class="form-group">
					<input type="submit" value="submit" class="btn btn-block btn-primary">
				</div>
			</form>
		</div>
	</div>
