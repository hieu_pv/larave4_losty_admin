@extends('admin.template')
@section('content')
<script type="text/javascript">
	var items = {{$items}};
	var pages = {{json_encode($pages)}};
	var TOTAL_ITEM = {{$total_item}};
	var STR_REMOVE_ITEM = "{{asset('ajax/remove-item')}}";
</script>
<script type="text/javascript" src="{{asset('lib/js/admin/item.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('lib/css/admin/item.css')}}">
<div class="container" ng-controller="ItemCtrl">
	<p><strong>Total: </strong><span ng-bind="TOTAL_ITEM"></span></p>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td class="text-center">Image</td>
				<td>Title</td>
				<td>User</td>
				<td>Description</td>
				<td>Category</td>
				<td>Time</td>
				<td class="text-center">Report</td>
				<td class="text-center">Delete</td>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="item in items">
				<td><img class="item_img center-block" ng-src="<%item.image_link%>"></td>
				<td ng-bind="item.title"></td>
				<td ng-bind="item.user.username"></td>
				<td ng-bind="item.description"></td>
				<td ng-bind="item.category"></td>
				<td ng-bind="item.time_post"></td>
				<td class="text-center" ng-bind="item.report"></td>
				<td class="text-center"><span class="glyphicon glyphicon-remove" ng-click="RemoveItem(item)"></span></td>
			</tr>
		</tbody>
	</table> <!-- table -->
	<div class="row">
		<ul class="pagination">
			<li ng-repeat="page in pages">
				<a ng-href="<%page.link%>" ng-bind="page.key"></a>
			</li>
		</ul>
	</div>
</div> <!-- container -->

@stop