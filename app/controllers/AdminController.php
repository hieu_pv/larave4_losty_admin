<?php
class AdminController extends Controller{
	protected $user;
	protected $category;
	protected $item;
	public function __construct(UserRepository $user,CategoryRepository $category,ItemRepository $item)
	{
		$this->user = $user;
		$this->category = $category;
		$this->item = $item;
	}
	public function getLogin(){
		return View::make('admin.login')->with('title','Login');
	}
	public function postLogin()
	{
		extract(Input::only('username','password'));
		if($username == 'admin' && $password == 'losty'){
			Session::put('login',1);
			return Redirect::to('/');
		}else{
			return Redirect::to('/login')->with('error','Username or Password not match');
		}
	}
	public function getLogout()
	{
		Session::forget('login');
		return Redirect::to('login');
	}
	public function getIndex()
	{
		return View::make('admin.index')->with('title','Admin');
	}
	public function getUser()
	{
		$number_item_per_page = 20;
		$total_item           = User::count();
		$page = App::make('BaseController')->page('page',$number_item_per_page,$total_item);
		$data = [
			'users'     => $this->user->getUser($page->skip,$page->take),
			'title'     => 'Mange User',
			'pages' => $page->pages,
		];
		return View::make('admin.user',$data);
	}
	public function getCategory()
	{
		$data = [
			'title'      => 'Manage Category',
			'categories' => $this->category->getAllCategories(),
		];
		return View::make('admin.category',$data);
	}
	public function getItem()
	{	
		$total_item = Item::count();

		$page = App::make('BaseController')->page('page',20,$total_item);

		$items = $this->item->getItem($page->skip,$page->take); 

		$data = [
			'title'      => 'Manage Item',
			'items'      => $items,
			'pages'      => $page->pages,
			'total_item' => $total_item,
		];
		return View::make('admin.item',$data);
	}
}
?>