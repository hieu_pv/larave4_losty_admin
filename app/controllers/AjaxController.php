<?php
class AjaxController extends Controller{
	protected $user;
	protected $category;
	protected $item;
	public function __construct(UserRepository $user,CategoryRepository $category,ItemRepository $item)
	{
		$this->user = $user;
		$this->category = $category;
		$this->item = $item;
	}
	public function postChangeUserStatus(){
		try {
			extract(Input::only('user_id','status'));
			$this->user->ChangeUserStatus($user_id,$status);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postChangeUserPermission(){
		try {
			extract(Input::only('user_id','permission'));
			$this->user->ChangeUserPermission($user_id,$permission);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postEditUsername(){
		try {
			extract(Input::only('user_id','username'));
			$this->user->EditUsername($user_id,$username);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postEditEmail(){
		try {
			extract(Input::only('user_id','email'));
			$this->user->EditEmail($user_id,$email);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postRemoveCategory(){
		try {
			extract(Input::only('category_id'));
			$this->category->RemoveCategory($category_id);
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postEditCategory(){
		try {
			extract(Input::only('category_id','name'));
			$this->category->EditCategory($category_id,$name);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postNewCategory(){
		try {
			extract(Input::only('name'));
			$this->category->NewCategory($name);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}
	public function postRemoveItem(){
		try {
			extract(Input::only('item_id'));
			$this->item->RemoveItem($item_id);
			return ['status' => STR_STATUS_SUCCESS];
		} catch (Exception $e) {
			return ['status' => STR_STATUS_ERROR, 'message' => $e->getMessage()];
		}
	}

}
?>