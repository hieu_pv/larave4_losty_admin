<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	public static function page($get_page = 'page',$number_item_per_page = 10,$total_item = 50){
		$page = new stdClass();
		if(!isset($_GET[$get_page])){
			$page->page = 1;
		}else{
			$rules = [
				'page' => 'regex:/^[0-9]+$/'
			];
			$valid = [
				'page' => $_GET[$get_page]
			];
			if(Validator::make($valid,$rules)->fails())
				$page->page = 1;
			else
				$page->page = $_GET[$get_page];
		}
		$total_page = ceil($total_item/$number_item_per_page);
		$page->skip = ($page->page - 1)*$number_item_per_page;
		$page->take = $number_item_per_page;
		$pages = [];
		for ($i=1; $i <= $total_page ; $i++) { 
			$page_link = new stdClass;
			$page_link->key = $i;
			$page_link->link = Request::url().'?'.$get_page.'='.$i;
			array_push($pages, $page_link);
		}
		$page->pages = $pages;
		return $page;
	}

}
