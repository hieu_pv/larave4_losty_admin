Losty.controller('CategoryCtrl',function($scope,$http){
	$scope.categories = categories;

	/*
	* remove Category
	*/
	$scope.RemoveCategory = function(category){
		var r = confirm('Are you sure you want delete this category');
		if(r == false){
			return;
		}
		var data = {
			category_id : category._id,
		};
		$http.post(STR_URL_REMOVE_CATEGORY,data).success(function(response){
			document.location.reload();
		})
	}
	$scope.EditCategory = function(category){
		var new_category = prompt('Enter new category\'s name');
		if(new_category == undefined || new_category == '' || new_category == category.name){
			return;
		}
		var data = {
			category_id : category._id,
			name : new_category
		};
		$http.post(STR_URL_EDIT_CATEGORY,data).success(function(response){
			document.location.reload();
		})
	}
	$scope.AddNewCategory = function(){
		var new_category = prompt('Enter new category');
		if(new_category == undefined || new_category == ''){
			return;
		}
		var data = {
			name : new_category
		};
		$http.post(STR_URL_ADD_NEW_CATEGORY,data).success(function(response){
			document.location.reload();
		})
	}
});