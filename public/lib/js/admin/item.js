Losty.controller('ItemCtrl',function($scope,$http){
	$scope.items = items;
	$scope.pages = pages; 
	$scope.TOTAL_ITEM = TOTAL_ITEM;

	/*
	* remove item
	*/
	$scope.RemoveItem = function(item){
		var r = confirm('Are you sure you want delete this item');
		if(r == false){
			return;
		}
		$http.post(STR_REMOVE_ITEM,{item_id: item._id}).success(function(response){
			document.location.reload();
		})
	}
})