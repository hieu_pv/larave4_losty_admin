Losty.controller('UserCtrl',function($scope,$http){
	$scope.users    = users;
	$scope.pages    = pages;
	$scope.ACTIVE   = ACTIVE;
	$scope.DEACTIVE = DEACTIVE;
	$scope.USER = USER;
	$scope.ADMIN = ADMIN;

	/*
	* Edit username
	*/
	$scope.EditUsername = function(user){
		var new_username = prompt('Enter new username');
		if(new_username == undefined || new_username == ''){
			return;
		}
		$http.post(STR_EDIT_USERNAME,{user_id: user._id,username: new_username}).success(function(response){
			if(response.status == SUCCESS){
				document.location.reload();
			}else{
				alert(response.message);
			}
		})
	}
	/*
	* Edit email
	*/
	$scope.EditEmail = function(user){
		var new_email = prompt('Enter new email');
		if(new_email == undefined || new_email == ''){
			return;
		}
		$http.post(STR_EDIT_EMAIL,{user_id: user._id,email: new_email}).success(function(response){
			if(response.status == SUCCESS){
				document.location.reload();
			}else{
				alert(response.message);
			}
		})
	}
	/*
	* change user's status ACTIVE = 1/DEACTIVE = 0
	*/
	$scope.ChangeUserStatus = function(user,status){
		var data = {user_id: user._id,status: status};
		$http.post(STR_URL_CHANGE_USER_STATUS,data).success(function(response){
			console.log(response);
			if(response.status == SUCCESS){
				var index = users.indexOf(user);
				users[index].active = status;
				$scope.users = users;
			}
		})
	}
	/*
	* change user permission
	*/
	$scope.ChangeUserPermission = function(user){
		var r = confirm('Change user permission');
		if(r == undefined || r == false){
			return;
		}
		if(user.permission == ADMIN){
			var permission = USER;
		}else{
			var permission = ADMIN;
		}
		var data = {user_id: user._id,permission: permission};
		console.log(JSON.stringify(data));
		$http.post(STR_CHANGE_USER_PERMISSION,data).success(function(response){
			document.location.reload();
		})
	}

})